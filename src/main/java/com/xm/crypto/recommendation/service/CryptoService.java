package com.xm.crypto.recommendation.service;

import com.xm.crypto.recommendation.dao.CryptoCalculateRepository;
import com.xm.crypto.recommendation.dao.CryptoRepository;
import com.xm.crypto.recommendation.dao.CryptoTypeRepository;
import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity;
import com.xm.crypto.recommendation.dao.model.CryptoEntity;
import com.xm.crypto.recommendation.exception.ErrorCode;
import com.xm.crypto.recommendation.exception.NotFoundException;
import com.xm.crypto.recommendation.mapper.CalculateMapper;
import com.xm.crypto.recommendation.mapper.CryptoMapper;
import com.xm.crypto.recommendation.mapper.CryptoTypeMapper;
import com.xm.crypto.recommendation.model.CryptoCalculateDetails;
import com.xm.crypto.recommendation.model.CryptoDto;
import com.xm.crypto.recommendation.model.CryptoFilter;
import com.xm.crypto.recommendation.model.CryptoTypeDto;
import com.xm.crypto.recommendation.model.CalculateFilter;
import com.xm.crypto.recommendation.specification.CryptoCalculateSpecification;
import com.xm.crypto.recommendation.specification.CryptoSpecification;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@EnableCaching
@RequiredArgsConstructor
public class CryptoService {
    private final CryptoRepository cryptoRepository;
    private final CryptoCalculateRepository cryptoCalculateRepository;
    private final CryptoTypeRepository cryptoTypeRepository;


    public List<CryptoDto> readAllCrypto(CryptoFilter cryptoFilter) {
        List<CryptoEntity> crypto = cryptoRepository.findAll(new CryptoSpecification(cryptoFilter));
        return CryptoMapper.INSTANCE.toDtoList(crypto);
    }

    public List<CryptoCalculateDetails> getCalculates(CalculateFilter filter) {

        LocalDate periodDate = cryptoRepository.maxDate().toLocalDate();
        filter.setPeriodDate(periodDate);

        if (Objects.isNull(filter.getPeriod())) {
            periodDate = filter.getPeriodDate().minusMonths(1).atStartOfDay().toLocalDate();
        } else {
            periodDate = filter.getPeriodDate().minusMonths(filter.getPeriod()).atStartOfDay().toLocalDate();
        }
        filter.setPeriodDate(periodDate);
        List<CryptoCalculateEntity> calculateEntityList = cryptoCalculateRepository.findAll(
                new CryptoCalculateSpecification(filter), Sort.by(Sort.Direction.DESC, "range"));
        return CalculateMapper.INSTANCE.toDtoList(calculateEntityList);
    }

    public CryptoCalculateDetails getCalculateBySymbol(String symbol) {
        CryptoCalculateEntity calculateEntityList = cryptoCalculateRepository.findBySymbol(symbol);
        return CalculateMapper.INSTANCE.toDto(calculateEntityList);
    }

    public CryptoCalculateDetails getCalculateRange(CalculateFilter filter) {
        var calculateEntityList = cryptoCalculateRepository.findAll(
                        new CryptoCalculateSpecification(filter), Sort.by(Sort.Direction.DESC, "range"))
                .stream().findFirst().orElseThrow(() -> new NotFoundException(
                        ErrorCode.RESOURCE_MISSING, "This date is not found any symbol"));
        return CalculateMapper.INSTANCE.toDto(calculateEntityList);
    }

    public CryptoTypeDto createCryptoType(CryptoTypeDto cryptoType) {
        var mapper = CryptoTypeMapper.INSTANCE.toEntity(cryptoType);
        return CryptoTypeMapper.INSTANCE.toDto(cryptoTypeRepository.save(mapper));
    }

}
