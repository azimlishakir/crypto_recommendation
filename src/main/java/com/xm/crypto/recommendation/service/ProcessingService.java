package com.xm.crypto.recommendation.service;

import com.xm.crypto.recommendation.dao.CryptoCalculateRepository;
import com.xm.crypto.recommendation.dao.CryptoRepository;
import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity;
import com.xm.crypto.recommendation.dao.model.CryptoEntity;
import com.xm.crypto.recommendation.queue.event.CalculateCryptoEvent;
import com.xm.crypto.recommendation.queue.event.ReadCryptoEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ProcessingService {
    private final CryptoCalculateRepository cryptoCalculateRepository;
    private final CryptoRepository cryptoRepository;

    public void processCalculateCryptoEvent(CalculateCryptoEvent event) {


        double range = (event.getMaxPrice() - event.getMinPrice()) / event.getMinPrice();

        cryptoCalculateRepository.save(CryptoCalculateEntity.builder()
                .minPrice(event.getMinPrice())
                .maxPrice(event.getMaxPrice())
                .symbol(event.getSymbol())
                .newest(event.getNewest())
                .oldest(event.getOldest())
                .range(range)
                .build());
    }

    public void processReadCryptoEvent(ReadCryptoEvent event) {

        var entity = CryptoEntity.builder()
                .price(event.getPrice())
                .timeStamp(event.getTimeStamp())
                .symbol(event.getSymbol())
                .build();

        cryptoRepository.save(entity);

    }
}
