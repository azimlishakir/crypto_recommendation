package com.xm.crypto.recommendation.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorCode {
    public static final String UNEXPECTED_INTERNAL_ERROR = "unexpected_internal_error";
    public static final String PARAMETER_INVALID = "parameter_invalid";
    public static final String REQUEST_INVALID = "request_body_invalid";
    public static final String RESOURCE_MISSING = "resource_missing";

}
