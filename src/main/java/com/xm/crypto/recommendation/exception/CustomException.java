package com.xm.crypto.recommendation.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class CustomException extends RuntimeException {
    private final String code;
    private final String message;
    private final Object validations;

    protected CustomException(String code, String message, Object validations) {
        super(code);
        this.code = code;
        this.message = message;
        this.validations = validations;
    }
}
