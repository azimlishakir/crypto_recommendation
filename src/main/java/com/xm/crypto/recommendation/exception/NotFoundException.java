package com.xm.crypto.recommendation.exception;

public class NotFoundException extends CustomException {
    public NotFoundException(String code, String message) {
        super(code, message, null);
    }
}
