package com.xm.crypto.recommendation.mapper;

import com.xm.crypto.recommendation.dao.model.CryptoTypeEntity;
import com.xm.crypto.recommendation.model.CryptoTypeDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CryptoTypeMapper {
    CryptoTypeMapper INSTANCE = Mappers.getMapper(CryptoTypeMapper.class);

    CryptoTypeDto toDto(CryptoTypeEntity cryptoTypeEntity);

    CryptoTypeEntity toEntity(CryptoTypeDto cryptoTypesEntity);
}
