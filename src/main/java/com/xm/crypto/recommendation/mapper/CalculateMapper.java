package com.xm.crypto.recommendation.mapper;

import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity;
import com.xm.crypto.recommendation.model.CryptoCalculateDetails;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CalculateMapper {

    CalculateMapper INSTANCE = Mappers.getMapper(CalculateMapper.class);

    CryptoCalculateDetails toDto(CryptoCalculateEntity calculateEntity);

    List<CryptoCalculateDetails> toDtoList(List<CryptoCalculateEntity> calculateEntityList);


}
