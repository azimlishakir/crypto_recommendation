package com.xm.crypto.recommendation.mapper;

import com.xm.crypto.recommendation.dao.model.CryptoEntity;
import com.xm.crypto.recommendation.model.CryptoDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CryptoMapper {
    CryptoMapper INSTANCE = Mappers.getMapper(CryptoMapper.class);

    CryptoDto toDto(CryptoEntity cryptoEntity);

    List<CryptoDto> toDtoList(List<CryptoEntity> cryptoEntityList);

}
