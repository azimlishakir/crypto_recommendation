package com.xm.crypto.recommendation.model;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalculateFilter {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate newest;


    private LocalDate periodDate;
    private Long period;
}
