package com.xm.crypto.recommendation.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptoDto {
    private LocalDateTime timeStamp;
    private String symbol;
    private Double price;
}
