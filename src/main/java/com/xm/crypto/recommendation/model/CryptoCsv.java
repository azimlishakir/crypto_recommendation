package com.xm.crypto.recommendation.model;

import com.opencsv.bean.CsvBindByName;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CryptoCsv {

    @CsvBindByName(column = "timestamp", locale = "en-GB")
    private Long timeStamp;
    @CsvBindByName(column = "symbol", locale = "en-GB")
    private String symbol;
    @CsvBindByName(column = "price", locale = "en-GB")
    private Double price;

    private LocalDateTime timeStampLocalDateTime;
}
