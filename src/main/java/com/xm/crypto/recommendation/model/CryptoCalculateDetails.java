package com.xm.crypto.recommendation.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CryptoCalculateDetails {

    private String symbol;
    private Double minPrice;
    private Double maxPrice;
    private LocalDateTime oldest;
    private LocalDateTime newest;
    private Double range;
}
