package com.xm.crypto.recommendation.util;

import com.opencsv.bean.CsvToBeanBuilder;
import com.xm.crypto.recommendation.model.CryptoCsv;
import java.io.File;
import java.io.FileReader;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import lombok.experimental.UtilityClass;
import org.springframework.util.ResourceUtils;

@UtilityClass
public final class CryptoUtil {

    public static List<CryptoCsv> readCvv(String path) {

        File file;
        List<CryptoCsv> beans = new ArrayList<>();
        try {
            file = ResourceUtils.getFile("classpath:".concat(path));
            beans = new CsvToBeanBuilder(new FileReader(file))
                    .withType(CryptoCsv.class)
                    .build()
                    .parse();


            for (CryptoCsv cryptoCsv : beans) {
                var ld = LocalDateTime.ofInstant(Instant.ofEpochMilli(cryptoCsv.getTimeStamp()), TimeZone
                        .getDefault().toZoneId());
                cryptoCsv.setTimeStampLocalDateTime(ld);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return beans;
    }

}
