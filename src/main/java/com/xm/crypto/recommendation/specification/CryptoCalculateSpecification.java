package com.xm.crypto.recommendation.specification;

import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity;
import com.xm.crypto.recommendation.model.CalculateFilter;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;


public class CryptoCalculateSpecification implements Specification<CryptoCalculateEntity> {

    private final CalculateFilter filter;

    public CryptoCalculateSpecification(CalculateFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<CryptoCalculateEntity> root, CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (!ObjectUtils.isEmpty(filter.getNewest())) {
            predicates.add(criteriaBuilder.lessThan(root.get("newest").as(LocalDateTime.class),
                    LocalDateTime.of(filter.getNewest(), LocalTime.of(0, 0))));
        }

        if (!ObjectUtils.isEmpty(filter.getPeriodDate())) {
            predicates.add(criteriaBuilder.greaterThan(root.get("newest").as(LocalDateTime.class),
                    LocalDateTime.of(filter.getPeriodDate(), LocalTime.of(0, 0))));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
