package com.xm.crypto.recommendation.specification;

import com.xm.crypto.recommendation.dao.model.CryptoEntity;
import com.xm.crypto.recommendation.model.CryptoFilter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

public class CryptoSpecification implements Specification<CryptoEntity> {
    private final CryptoFilter cryptoFilter;

    public CryptoSpecification(CryptoFilter cryptoFilter) {
        this.cryptoFilter = cryptoFilter;
    }

    @Override
    public Predicate toPredicate(Root<CryptoEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (!ObjectUtils.isEmpty(cryptoFilter.getSymbol())) {
            predicates.add(criteriaBuilder.equal(root.get("symbol"),
                    cryptoFilter.getSymbol()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
