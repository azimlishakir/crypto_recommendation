package com.xm.crypto.recommendation.controller;

import com.xm.crypto.recommendation.model.CryptoCalculateDetails;
import com.xm.crypto.recommendation.model.CryptoDto;
import com.xm.crypto.recommendation.model.CryptoFilter;
import com.xm.crypto.recommendation.model.CryptoTypeDto;
import com.xm.crypto.recommendation.model.CalculateFilter;
import com.xm.crypto.recommendation.service.CryptoService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/crypto")
public class CryptoController {

    private final CryptoService cryptoService;

    @GetMapping("/read")
    @Operation(summary = "Get all of them cryptos grt by symbol")
    public ResponseEntity<List<CryptoDto>> readAllCrypto(CryptoFilter cryptoFilter) {
        return ResponseEntity.ok(cryptoService.readAllCrypto(cryptoFilter));
    }

    @GetMapping("/calculates")
    @Operation(summary = "Get sorted list of all the cryptos, comparing the range")
    public ResponseEntity<List<CryptoCalculateDetails>> getCalculates(CalculateFilter filter) {
        return ResponseEntity.ok(cryptoService.getCalculates(filter));
    }

    @GetMapping("/calculate-symbol")
    @Operation(summary = "Get values for a requested crypto")
    public ResponseEntity<CryptoCalculateDetails> getCalculateBySymbol(@RequestParam String symbol) {
        return ResponseEntity.ok(cryptoService.getCalculateBySymbol(symbol));
    }

    @GetMapping("/calculate-range")
    @Operation(summary = "Get crypto with the range for a specific day")
    public ResponseEntity<CryptoCalculateDetails> getCalculateRange(CalculateFilter filter) {
        return ResponseEntity.ok(cryptoService.getCalculateRange(filter));
    }


    @PostMapping("/create")
    @Operation(summary = "Create crypto symbol(type)")
    public ResponseEntity<CryptoTypeDto> createCryptoType(@RequestBody CryptoTypeDto cryptoType) {
        return ResponseEntity.ok(cryptoService.createCryptoType(cryptoType));
    }

}
