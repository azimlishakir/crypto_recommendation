package com.xm.crypto.recommendation.queue;



import com.xm.crypto.recommendation.queue.event.CalculateCryptoEvent;
import com.xm.crypto.recommendation.queue.event.ReadCryptoEvent;
import com.xm.crypto.recommendation.service.ProcessingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class MessageListener {

    private final ProcessingService processingService;

    @RabbitListener(queues = "${spring.rabbitmq.queue.calculate-crypto}")
    public void calculateCrypto(CalculateCryptoEvent event) {
        log.info("Start processing of calculate-crypto event {}", event);
        processingService.processCalculateCryptoEvent(event);
        log.info("End processing of calculate-crypto event {}", event);
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue.read-crypto}")
    public void readCrypto(ReadCryptoEvent event) {
        log.info("Start processing of read-crypto event {}", event);
        processingService.processReadCryptoEvent(event);
        log.info("End processing of read-crypto event {}", event);
    }
}
