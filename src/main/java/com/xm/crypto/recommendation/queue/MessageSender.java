package com.xm.crypto.recommendation.queue;

import com.xm.crypto.recommendation.queue.event.CalculateCryptoEvent;
import com.xm.crypto.recommendation.queue.event.ReadCryptoEvent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MessageSender {
    private final RabbitTemplate rabbitTemplate;
    private final String calculateCryptoQ;
    private final String readCryptoQ;

    public MessageSender(RabbitTemplate rabbitTemplate,
                         @Value("${spring.rabbitmq.queue.calculate-crypto}") String calculateCryptoQ,
                         @Value("${spring.rabbitmq.queue.read-crypto}") String readCryptoQ) {
        this.rabbitTemplate = rabbitTemplate;
        this.calculateCryptoQ = calculateCryptoQ;
        this.readCryptoQ = readCryptoQ;
    }

    public void sendCalculateCryptoQ(CalculateCryptoEvent event) {
        rabbitTemplate.convertAndSend(calculateCryptoQ, event);
    }

    public void sendReadCryptoQ(ReadCryptoEvent event) {
        rabbitTemplate.convertAndSend(readCryptoQ, event);
    }
}
