package com.xm.crypto.recommendation.queue.event;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalculateCryptoEvent {
    private String symbol;
    private LocalDateTime newest;
    private LocalDateTime oldest;
    private Double minPrice;
    private Double maxPrice;
}
