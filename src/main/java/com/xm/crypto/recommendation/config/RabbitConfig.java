package com.xm.crypto.recommendation.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    @Qualifier("card-prolongation")
    public Queue queueCalculateCrypto(@Value("${spring.rabbitmq.queue.calculate-crypto}") String queueName) {
        return new Queue(queueName);
    }

    @Bean
    @Qualifier("card-prolongation")
    public Queue queueReadCrypto(@Value("${spring.rabbitmq.queue.read-crypto}") String queueName) {
        return new Queue(queueName);
    }

    @Bean
    public MessageConverter jsonConverter() {
        ObjectMapper mapper = new ObjectMapper()
                .findAndRegisterModules()
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return new Jackson2JsonMessageConverter(mapper);
    }
}
