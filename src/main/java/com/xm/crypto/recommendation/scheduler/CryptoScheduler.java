package com.xm.crypto.recommendation.scheduler;

import com.xm.crypto.recommendation.dao.CryptoCalculateRepository;
import com.xm.crypto.recommendation.dao.CryptoRepository;
import com.xm.crypto.recommendation.dao.CryptoTypeRepository;
import com.xm.crypto.recommendation.dao.model.CryptoTypeEntity;
import com.xm.crypto.recommendation.exception.ErrorCode;
import com.xm.crypto.recommendation.exception.NotFoundException;
import com.xm.crypto.recommendation.model.CryptoCalculateDetails;
import com.xm.crypto.recommendation.model.CryptoCsv;
import com.xm.crypto.recommendation.queue.MessageSender;
import com.xm.crypto.recommendation.queue.event.CalculateCryptoEvent;
import com.xm.crypto.recommendation.queue.event.ReadCryptoEvent;
import com.xm.crypto.recommendation.util.CryptoUtil;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@EnableScheduling
@RequiredArgsConstructor
public class CryptoScheduler {

    private final CryptoCalculateRepository cryptoCalculateRepository;
    private final CryptoRepository cryptoRepository;
    private final CryptoTypeRepository cryptoTypeRepository;
    private final MessageSender messageSender;

    @SchedulerLock(name = "calculatePrice", lockAtLeastFor = "PT5M", lockAtMostFor = "PT1H")
    @Scheduled(cron = "0 0 10 1/1 * ?", zone = "GMT+4")
    public void calculatePrice() {
        List<String> cryptoTypes = cryptoTypeRepository.findAll().stream().map(
                CryptoTypeEntity::getSymbol).collect(Collectors.toList());
        for (String ct : cryptoTypes) {
            var cryptoTypesEntity = cryptoTypeRepository.findBySymbol(ct)
                    .orElseThrow(() -> new NotFoundException(ErrorCode.RESOURCE_MISSING, "Crypto type not found"));

            var cryptoList = CryptoUtil.readCvv(cryptoTypesEntity.getPath());
            CryptoCalculateDetails calculateDetail = CryptoCalculateDetails.builder()
                    .symbol(ct)
                    .minPrice(cryptoList.stream()
                            .min(Comparator.comparing(CryptoCsv::getPrice)).orElseThrow().getPrice())
                    .maxPrice(cryptoList.stream()
                            .max(Comparator.comparing(CryptoCsv::getPrice)).orElseThrow().getPrice())
                    .oldest(cryptoList.stream().min(Comparator.comparing(CryptoCsv::getPrice))
                            .orElseThrow().getTimeStampLocalDateTime())
                    .newest(cryptoList.stream().max(Comparator.comparing(CryptoCsv::getPrice))
                            .orElseThrow().getTimeStampLocalDateTime())
                    .build();

            var event = CalculateCryptoEvent.builder()
                    .minPrice(calculateDetail.getMinPrice())
                    .maxPrice(calculateDetail.getMaxPrice())
                    .symbol(ct)
                    .newest(calculateDetail.getNewest())
                    .oldest(calculateDetail.getOldest())
                    .build();

            boolean check = cryptoCalculateRepository.existsByNewestOrOldest(event.getNewest(), event.getOldest());
            if (!check) {
                messageSender.sendCalculateCryptoQ(event);
            }
        }
    }


    @SchedulerLock(name = "readCsv", lockAtLeastFor = "PT5M", lockAtMostFor = "PT1H")
    @Scheduled(cron = "0 0 09 1/1 * ?", zone = "GMT+4")
    public void readCsv() {
        List<String> cryptoTypes = cryptoTypeRepository.findAll().stream().map(
                CryptoTypeEntity::getSymbol).collect(Collectors.toList());
        CryptoTypeEntity cryptoTypeEntity;

        for (String ct : cryptoTypes) {
            cryptoTypeEntity = cryptoTypeRepository.findBySymbol(ct)
                    .orElseThrow(() -> new NotFoundException(ErrorCode.RESOURCE_MISSING, "Crypto type not found"));
            List<CryptoCsv> mapper = CryptoUtil.readCvv(cryptoTypeEntity.getPath());
            for (CryptoCsv cryptoCsv : mapper) {
                ReadCryptoEvent event = ReadCryptoEvent.builder()
                        .timeStamp(cryptoCsv.getTimeStampLocalDateTime())
                        .price(cryptoCsv.getPrice())
                        .symbol(cryptoCsv.getSymbol())
                        .build();
                boolean check = cryptoRepository.existsByTimeStamp(event.getTimeStamp());
                if (!check) {
                    messageSender.sendReadCryptoQ(event);

                }
            }
        }
    }
}
