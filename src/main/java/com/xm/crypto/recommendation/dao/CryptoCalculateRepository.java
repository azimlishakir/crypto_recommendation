package com.xm.crypto.recommendation.dao;

import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity;
import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CryptoCalculateRepository extends JpaRepository<CryptoCalculateEntity, Long>,
        JpaSpecificationExecutor<CryptoCalculateEntity> {
    CryptoCalculateEntity findBySymbol(String symbol);

    boolean existsByNewestOrOldest(LocalDateTime newest, LocalDateTime oldest);

}
