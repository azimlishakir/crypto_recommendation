package com.xm.crypto.recommendation.dao;

import com.xm.crypto.recommendation.dao.model.CryptoEntity;
import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CryptoRepository extends JpaRepository<CryptoEntity, Long>, JpaSpecificationExecutor<CryptoEntity> {

    boolean existsByTimeStamp(LocalDateTime timeStamp);

    @Query(value = "select max(time_stamp) from crypto", nativeQuery = true)
    LocalDateTime maxDate();
}
