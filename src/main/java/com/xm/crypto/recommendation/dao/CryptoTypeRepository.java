package com.xm.crypto.recommendation.dao;

import com.xm.crypto.recommendation.dao.model.CryptoTypeEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoTypeRepository extends JpaRepository<CryptoTypeEntity, Long> {
    Optional<CryptoTypeEntity> findBySymbol(String symbol);
}
