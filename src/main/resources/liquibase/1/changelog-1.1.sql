INSERT INTO crypto_types (symbol, path)
VALUES ('BTC', 'csv/BTC_values.csv'),
       ('DOGE', 'csv/DOGE_values.csv'),
       ('ETH', 'csv/ETH_values.csv'),
       ('LTC', 'csv/LTC_values.csv'),
       ('XRP', 'csv/XRP_values.csv');