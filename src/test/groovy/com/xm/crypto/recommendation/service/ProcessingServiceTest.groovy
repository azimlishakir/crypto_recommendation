package com.xm.crypto.recommendation.service

import com.xm.crypto.recommendation.dao.CryptoCalculateRepository
import com.xm.crypto.recommendation.dao.CryptoRepository
import com.xm.crypto.recommendation.queue.event.CalculateCryptoEvent
import com.xm.crypto.recommendation.queue.event.ReadCryptoEvent
import spock.lang.Specification

import java.time.LocalDateTime

class ProcessingServiceTest extends Specification {

    CryptoCalculateRepository calculateRepository
    CryptoRepository cryptoRepository
    ProcessingService processingService

    void setup() {
        calculateRepository = Mock()
        cryptoRepository = Mock()
        processingService = new ProcessingService(calculateRepository, cryptoRepository)
    }

    def "ProcessCalculateCryptoEvent"() {

        CalculateCryptoEvent event = CalculateCryptoEvent.builder()
            .symbol("DOGE")
            .newest(LocalDateTime.now())
            .oldest(LocalDateTime.now())
            .maxPrice(1.2)
            .minPrice(1.2)
        .build()

        when:
        processingService.processCalculateCryptoEvent(event)

        then:
        1 * calculateRepository.save(*_)

    }

    def "ProcessReadCryptoEvent"() {

        given:
        ReadCryptoEvent event = ReadCryptoEvent.builder()
                .symbol("DOGE")
                .price(1)
                .build()
        when:
        processingService.processReadCryptoEvent(event)

        then:
        1 * cryptoRepository.save(*_)
    }
}
