package com.xm.crypto.recommendation.service

import com.xm.crypto.recommendation.dao.CryptoCalculateRepository
import com.xm.crypto.recommendation.dao.CryptoRepository
import com.xm.crypto.recommendation.dao.CryptoTypeRepository
import com.xm.crypto.recommendation.dao.model.CryptoCalculateEntity
import com.xm.crypto.recommendation.model.CalculateFilter
import com.xm.crypto.recommendation.model.CryptoFilter
import com.xm.crypto.recommendation.model.CryptoTypeDto
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class CryptoServiceTest extends Specification {

    CryptoRepository cryptoRepository
    CryptoCalculateRepository calculateRepository
    CryptoTypeRepository cryptoTypesRepository
    CryptoService cryptoService


    def setup() {
        cryptoRepository = Mock()
        calculateRepository = Mock()
        cryptoTypesRepository = Mock()
        cryptoService = new CryptoService(cryptoRepository, calculateRepository, cryptoTypesRepository)
    }

    def "ReadAllCrypto"() {
        given:
        CryptoFilter filter = CryptoFilter.builder()
                .symbol("DOGE")
                .build()
        when:
        cryptoService.readAllCrypto(filter)

        then:
        1 * cryptoRepository.findAll(*_)
    }

    def "GetCalculates"() {
        given:
        LocalDate periodDate = LocalDate.now()

        CalculateFilter filter = CalculateFilter.builder()
                .period(1)
                .periodDate(periodDate)
                .build()
        when:
        cryptoService.getCalculates(filter)

        then:
        1 * cryptoRepository.maxDate() >> LocalDateTime.now()
        1 * calculateRepository.findAll(*_)
    }

    def "getCalculateBySymbol"() {
        given:
        String symbol = "DOGE"
        when:
        cryptoService.getCalculateBySymbol(symbol)

        then:
        1 * calculateRepository.findBySymbol(symbol)
    }

    def "GetCalculateRange"() {
        given:
        CalculateFilter filter = CalculateFilter.builder()
                .newest(LocalDate.of(2021, 02, 12))
                .build()

        CryptoCalculateEntity calculate = CryptoCalculateEntity.builder()
                .newest(LocalDateTime.of(2021, 02, 12, 0, 0))
                .oldest(LocalDateTime.of(2021, 02, 12, 0, 0))
                .minPrice(1.1)
                .maxPrice(1.1)
                .range(2.0)
                .symbol("sybol")
                .id(1L)
                .build()

        List<CryptoCalculateEntity> calculateEntities = new ArrayList<>()
        calculateEntities.add(calculate)


        when:
        cryptoService.getCalculateRange(filter)

        then:
        1 * calculateRepository.findAll(*_) >> List.of(calculate)
    }

    def "CreateCryptoType"() {
        CryptoTypeDto filter = CryptoTypeDto.builder()
                .symbol("DOGE")
                .path("path")
                .build()


        when:
        cryptoService.createCryptoType(filter)

        then:
        1 * cryptoTypesRepository.save(*_)
    }
}
