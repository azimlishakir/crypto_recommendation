package com.xm.crypto.recommendation.controller

import com.xm.crypto.recommendation.exception.ErrorHandler
import com.xm.crypto.recommendation.service.CryptoService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class CryptoControllerTest extends Specification {

    CryptoService cryptoService
    CryptoController cryptoController
    MockMvc mockMvc

    String url = "/crypto"


    def setup() {
        cryptoService = Mock(CryptoService)
        cryptoController = new CryptoController(cryptoService)
        mockMvc = MockMvcBuilders.standaloneSetup(cryptoController).setControllerAdvice(new ErrorHandler()).build()
    }

    def "readAllCrypto"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url.concat("/read"))
                .contentType(MediaType.APPLICATION_JSON)
                .param("symbol", "DOGE")).andReturn().response
        then:
        1 * cryptoService.readAllCrypto(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "getCalculates"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url.concat("/calculates"))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * cryptoService.getCalculates(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "getCalculateBySymbol"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url.concat("/calculate-symbol"))
                .param("symbol","DOGE")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * cryptoService.getCalculateBySymbol(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "getCalculateRange"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url.concat("/calculate-range"))
                .param("period","1")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * cryptoService.getCalculateRange(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "createCryptoType"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.post(url.concat("/create"))
                .content("{\n" +
                        "    \"symbol\": \"DOGE\",\n" +
                        "    \"path\": \"path\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * cryptoService.createCryptoType(*_)
        assert response.status == HttpStatus.OK.value()
    }
}
